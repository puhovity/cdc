#include <iostream>
#include "Interfaces.h"
#include "Calculator.h"
#include "MyCar.h"
#include "MyDoc.h"

int main() {

    docsContainer docs;
    carsContainer cars;

    MyCar car1(1, 1, 2);
    MyCar car2(2, 2, 1);
    MyCar car3(6, 0, 15);

    // эти 2 подберет первая
    MyDoc doc1(1, 1.5, 1);
    MyDoc doc2(1, 0.5, 1);

    // этот для второй
    MyDoc doc3(2, 2.5, 1);

    // а эти для третей - они очень далеко
    MyDoc doc4(0, 0, 2);
    MyDoc doc5(1.0, 0.0, 5);
    MyDoc doc6(-10, -10, 5);

    cars.push_back(&car1);
    cars.push_back(&car2);
    cars.push_back(&car3);

    docs.push_back(&doc1);
    docs.push_back(&doc2);
    docs.push_back(&doc3);
    docs.push_back(&doc4);
    docs.push_back(&doc5);
    docs.push_back(&doc6);

    Calculator calc;
    routesContainer routes = calc.calcRoutes(docs, cars);
    for(routesContainer::iterator rIt = routes.begin(); rIt != routes.end(); rIt++) {
        std::cout << "New route" << std::endl;
        docsContainer routeDocs = (*rIt)->getDocs();
        for(docsContainer::iterator dIt = routeDocs.begin(); dIt != routeDocs.end(); dIt++) {
            std::cout << "Lat " << (*dIt)->Lat() << "; Lon " << (*dIt)->Lon() << "; Weight " << (*dIt)->Weight() << std::endl;
        }
    }

    return 0;
}