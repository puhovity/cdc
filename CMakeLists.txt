cmake_minimum_required(VERSION 3.3)
project(cdc)

set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11")

set(SOURCE_FILES main.cpp Interfaces.h Calculator.cpp Calculator.h MyCar.cpp MyCar.h MyDoc.cpp MyDoc.h MyRoute.cpp MyRoute.h MyCar2.cpp MyCar2.h)
add_executable(cdc_test ${SOURCE_FILES})
