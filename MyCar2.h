//
// Created by puhovity on 31.03.17.
//

#ifndef CDC_MYCAR2_H
#define CDC_MYCAR2_H
#include "Interfaces.h"
#include "MyRoute.h"

class MyCar2 : public ICar {
public:
    double StartLat();
    double StartLon();
    double MaxWeight();
    IRoute& CreateRoute();

private:
    MyRoute mRoute;
};


#endif //CDC_MYCAR2_H
