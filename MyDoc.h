//
// Created by puhovity on 30.03.17.
//

#ifndef CDC_MYDOC_H
#define CDC_MYDOC_H
#include "Interfaces.h"

class MyDoc : public IDoc {
public:
    MyDoc() : mLon(0), mLat(0), mWeight(0) {};
    MyDoc(double Lon, double Lat, double Weight) : mLon(Lon), mLat(Lat), mWeight(Weight) {};
    double Lon();
    double Lat();
    double Weight();
private:
    double mLon;
    double mLat;
    double mWeight;
};


#endif //CDC_MYDOC_H
