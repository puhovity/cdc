//
// Created by puhovity on 30.03.17.
//

#ifndef CDC_MYCAR_H
#define CDC_MYCAR_H
#include "Interfaces.h"
#include "MyRoute.h"
class MyCar : public ICar {
public:
    MyCar() : mStartLat(0), mStartLon(0), mMaxWeight(0) {};
    MyCar(double StartLat, double StartLon, double MaxWeight) : mStartLat(StartLat), mStartLon(StartLon), mMaxWeight(MaxWeight) {};
    double StartLat();
    double StartLon();
    double MaxWeight();
    IRoute& CreateRoute();

private:
    MyRoute mRoute;
    double mStartLat;
    double mStartLon;
    double mMaxWeight;
};

#endif //CDC_MYCAR_H
