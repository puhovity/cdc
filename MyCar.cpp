//
// Created by puhovity on 30.03.17.
//

#include "MyCar.h"

double MyCar::StartLat() {
    return mStartLat;
}

double MyCar::StartLon() {
    return mStartLon;
}

double MyCar::MaxWeight() {
    return mMaxWeight;
}

IRoute &MyCar::CreateRoute() {
    return mRoute;
}