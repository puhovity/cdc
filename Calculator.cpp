//
// Created by puhovity on 30.03.17.
//

#include "Calculator.h"
#include <math.h>

routesContainer Calculator::calcRoutes(docsContainer docs, carsContainer cars) {

    routesContainer resultContainer;

    // перебираем машины
    for(carsContainer::iterator carIt = cars.begin(); carIt != cars.end() && !docs.empty(); carIt++) {

        // координаты последней точки (сначала совпадает с положением машины)
        double currentLat = (*carIt)->StartLat();
        double currentLon = (*carIt)->StartLon();
        double currentWeight = 0;

        IRoute* carRoute = &((*carIt)->CreateRoute());

        // пока машина не перегружена и есть доступные документы, ищем ближайший документ и закидываем в машину, если он еще влезет, затем удаляем документ из списка доступных
        while(currentWeight <= (*carIt)->MaxWeight() && !docs.empty()) {

            // ищем ближайший документ
            docsContainer::iterator closestDoc = docs.begin();
            double lastDistance = pow(currentLat - (*closestDoc)->Lat(), 2) + pow(currentLon - (*closestDoc)->Lon(), 2);
            for(docsContainer::iterator docIt = docs.begin(); docIt != docs.end(); docIt++) {
                double currentDistance = pow(currentLat - (*docIt)->Lat(), 2) + pow(currentLon - (*docIt)->Lon(), 2);
                if (currentDistance < lastDistance) {
                    closestDoc = docIt;
                    lastDistance = currentDistance;
                }
            }

            // добавляем, если еще есть место, либо больше не ищем и переходим к другой машине
            if ((currentWeight + (*closestDoc)->Weight()) <= (*carIt)->MaxWeight()) {
                carRoute->addDoc(*closestDoc);
                currentWeight += (*closestDoc)->Weight();
                currentLat = (*closestDoc)->Lat();
                currentLon = (*closestDoc)->Lon();
                docs.erase(closestDoc);
            }   else {
                break;
            }
        }

        resultContainer.push_back(carRoute);
    }

    return resultContainer;
}