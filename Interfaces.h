//
// Created by puhovity on 30.03.17.
//

#ifndef CDC_IDOC_H
#define CDC_IDOC_H
#include <vector>
#include <list>

class IDoc {
    public:
        virtual double Lat() {};
        virtual double Lon() {};
        virtual double Weight() {};
};

typedef std::vector<IDoc*> docsContainer;

class IRoute {
    public:
        virtual docsContainer getDocs() {};
        virtual void addDoc(IDoc* doc) {};
};
typedef std::vector<IRoute*> routesContainer;

class ICar {
    public:
        virtual double StartLat() {};
        virtual double StartLon() {};
        virtual double MaxWeight() {};
        virtual IRoute& CreateRoute() {};
};

typedef std::vector<ICar*> carsContainer;

#endif //CDC_IDOC_H
