//
// Created by puhovity on 30.03.17.
//

#ifndef CDC_CALCULATOR_H
#define CDC_CALCULATOR_H

#include "Interfaces.h"

class Calculator {
    public:
        routesContainer calcRoutes(docsContainer docs, carsContainer cars);
};

#endif //CDC_CALCULATOR_H
