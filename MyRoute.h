//
// Created by puhovity on 30.03.17.
//

#ifndef CDC_MYROUTE_H
#define CDC_MYROUTE_H
#include "Interfaces.h"

class MyRoute : public IRoute {
public:
    docsContainer getDocs();
    void addDoc(IDoc* doc);
private:
    docsContainer mContainer;
};


#endif //CDC_MYROUTE_H
