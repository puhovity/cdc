//
// Created by puhovity on 30.03.17.
//
#include "MyRoute.h"

docsContainer MyRoute::getDocs() {
    return mContainer;
}

void MyRoute::addDoc(IDoc* doc) {
    mContainer.push_back(doc);
}